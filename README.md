# PDK: the PIMS Development Kit

[PIMCity](https://www.pimcity-h2020.eu) is an H2020 project that is working to offer a set of tools to improve the data collection and economy.
With the PIMCity PIMS Development Kit (PDK), we offer basic and generic components that support the fundamental functionalities for Personal Information Management Systems (PIMS). These modules are released as a Software Development Kit (SDK), aiming to streamline the development and integration of PIMS. Our goal is to commoditize the complexity of creating PIMS to lower the barriers for companies to enter the web data market. The source code of all modules is available online on GitLab.

The PDK is organized in basic components that are necessary for building PIMS, as depicted below.

![PDK](https://gitlab.com/pimcity/pdk-description/-/raw/main/images/pdk.png)


The PDK includes all these basic components that use [Open APIs](https://www.openapis.org/) to enable communications and interactions among them, easing integration with existing PIMS, as well as the design and development of new ones. Open API specs are available [here](https://gitlab.com/pimcity/wp5/open-api).

## Tools to improve user's privacy

They include functionalities that allow the users to take informed decisions about which information to share and with whom. They are available [online](https://gitlab.com/pimcity/wp2).

### [Personal Data Safe (P-DS):](https://gitlab.com/pimcity/wp2/personal-data-safe)

<img src="https://gitlab.com/pimcity/pdk-description/-/raw/main/images/pds.png" width="250">

The Personal Data Safe (P-DS) is the means to store personal data in a controlled form. It implements a secure repository for the user&#39;s personal information like navigation history, contacts, preferences, personal information, etc.

### [Personal Privacy Metrics (P-PM):](https://gitlab.com/pimcity/wp2/privacy_metrics)

<img src="https://gitlab.com/pimcity/pdk-description/-/raw/main/images/ppm.png" width="250">

Privacy Metrics represent the means to increase the user&#39;s awareness. This component collects, computes and shares easy-to-understand data to allow users know how a service (e.g., a data buyer) stores and manages the data, if it shares it with third parties, etc. These are all fundamental pieces of information for a user to know to take informed decisions. The PM computes and offers this information via a standard interface, offering an open knowledge information system which can be queried using an open and standard platform. PMs combine information offered by services themselves, augmented by domain experts, volunteers, and contributors, and blended together via supervised machine learning analytics.

### [Personal Consent Manager (P-CM):](https://gitlab.com/pimcity/wp2/personal-consent-manager)

<img src="https://gitlab.com/pimcity/pdk-description/-/raw/main/images/pcm.png" width="250">

The Personal Consent Manager (P-CM) is the means to define all the user&#39;s preferences when dealing with personal data. It defines which data a service is allowed to collect, process, or which can be shared with third parties by managing explicit consent.

### [Personal Privacy Preserving Analytics (P-PPA):](https://gitlab.com/pimcity/wp2/personal-privacy-preserving-analytics)

<img src="https://gitlab.com/pimcity/pdk-description/-/raw/main/images/pppa.png" width="250">

The Personal Privacy Preserving Analytics (P-PPA) module has the goal of allowing data analysts and stakeholders to extract useful information from the raw data while preserving the privacy of the users whose data is in the datasets. It leverages concepts like [Differential Privacy](https://en.wikipedia.org/wiki/Differential_privacy) and [K-Anonymity](https://en.wikipedia.org/wiki/K-anonymity) so that data can be processed and shared while guaranteeing privacy for the users.



## Tools for the new data economy

Fundamental for PIMS is the creation of a transparent, open and easily accessible data market. We identify two fundamental components and functionalities for this. They are available [online](https://gitlab.com/pimcity/wp3).

### Data Valuation Tools (D-VT):

<img src="https://gitlab.com/pimcity/pdk-description/-/raw/main/images/dvt.png" width="250">

Data Valuation Tools consists two separate modules: The Data Valuation Tools from the market perspective ([DVTMP](https://gitlab.com/pimcity/wp3/dvtmp)) and The Data Valuation Tools from the user perspective ([DVTUP](https://gitlab.com/pimcity/wp3/dvtup)). The DVTMP module leverages some of the most popular existing online advertising platforms such as Facebook and Twitter to estimate the value of hundreds to thousands of audiences. The DVTMP module provides the monetary value of audiences traded on these platforms. This serves any PIM deciding to implement to have a realistic estimation of audiences to be traded. The DVTUP module provides estimated valuations of end-users&#39; data for the bulk dataset they are selling through the marketplace.

### [ Data Trading Engine (D-TE):](https://gitlab.com/pimcity/wp3/datatradingengine)

<img src="https://gitlab.com/pimcity/pdk-description/-/raw/main/images/dte.png" width="250">

The primary objective for the Data Trading Engine is to execute all transactions within the platform to exchange data for value in a secure, transparent, and fair-for-all way. Its key requirement is to be fully GDPR compliant. It must be able to receive Data Offers from Data Buyers and fulfil them with desired data, involving only those users that have proactively consented to share such data with such company for such specific purpose.



## Tools for data management

Data needs to be exported, imported and exchanged using standard mechanism, with proper metadata that let the system know the data source, data value, and facilitate the data aggregation from heterogenous sources. We offer four modules available [online](https://gitlab.com/pimcity/wp4).

### [ Data Aggregation (DA):](https://gitlab.com/pimcity/wp4/data-aggregation-api)

<img src="https://gitlab.com/pimcity/pdk-description/-/raw/main/images/da.png" width="250">

The Data Aggregation (DA) tool enables data owners that hold a bulk of their users&#39; data to perform two important processes on their data: aggregation and anonymization. This allows them to share these data in a privacy-preserving way. This module is intended to reside on the data owner&#39;s side.

### [ Data portability and control (DPC):](https://gitlab.com/pimcity/wp4/data-portability-control-tool)

<img src="https://gitlab.com/pimcity/pdk-description/-/raw/main/images/dpc.png" width="250">

The purpose of the Data Portability and Control (DPC) tool is to allow individual users to migrate their data to new platforms, in a privacy-preserving fashion. More specifically, it provides methods for extracting data from one PIMS (e.g., Bank data through the TrueLayer API), process it by filtering out sensitive information or user-inputted data (e.g., remove login credentials or debit card numbers), and outport it into other PDK module, a new PIMS (e.g., EasyPIMS) or an exported file in a common data interchange format, e.g., JSON.

### [ Data provenance (DP): ](https://gitlab.com/pimcity/wp4/data-provenance)

<img src="https://gitlab.com/pimcity/pdk-description/-/raw/main/images/dp.png" width="250">

The Data Provenance module OpenAPI allows developers to insert watermarks of ownership in the datasets they share in the marketplace. In general, this component is used internally by the PDK and developers that are in need of controlling data ownership even after a dataset has left the platform. This is done by embedding difficult to remove watermarks into the datasets.

### [  Data knowledge extraction (DKE):](https://gitlab.com/pimcity/wp4/userprofilingsystem)

<img src="https://gitlab.com/pimcity/pdk-description/-/raw/main/images/dke.png" width="250">

The Data Knowledge Extraction (DKE) component offers the means to extract knowledge from the raw data implementing machine learning and big data solutions. One of the biggest challenges here is the creation of value out of the raw data. When dealing with personal data, this must be coupled with privacy preserving approaches, so that only the necessary data are disclosed, and the data owner keeps the control on them. The DKE consists of machine learning approaches to aggregate data, abstract models to predict future data (e.g., predict user&#39;s interests in recommendation systems), fuse data coming from different sources to derive generic suggestions (e.g., to support decision by users, providing suggestions based on decisions taken by users with similar interests).
